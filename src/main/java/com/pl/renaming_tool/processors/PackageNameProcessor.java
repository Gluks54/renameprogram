package com.pl.renaming_tool.processors;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class PackageNameProcessor {
    private int maxDepth = 100;

    public List<Path> findPackages(Path pathOfRepository, String oldWord) throws IOException {
        return Files
                .find(pathOfRepository, maxDepth, (path, basicFileAttributes) -> {
                    File file = path.toFile();
                    return file.isDirectory() && file.getName().contains(oldWord);
                })
                .collect(Collectors.toList());
    }
}
