package com.pl.renaming_tool.processors;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClassAndContentProcessor {
    private int maxDepth = 100;

    public List<Path> findClasses(Path pathOfRepository, String oldWord) throws IOException {
        return Files
                .find(pathOfRepository, maxDepth, (path, basicFileAttributes) -> {
                    File file = path.toFile();
                    return !file.isDirectory() && file.getName().contains(oldWord);
                })
                .collect(Collectors.toList());
    }

    public List<Path> findFiles (Path pathOfRepository) throws IOException {
        return Files
                .find(pathOfRepository, maxDepth, (path, basicFileAttributes) -> {
                    File file = path.toFile();
                    return !file.isDirectory();
                })
                .collect(Collectors.toList());
    }

    public void renameContent(Path pathOfFile, String oldWord, String newWord) {
        try {
            Stream<String> lines = Files.lines(pathOfFile);

            List<String> replaced = lines
                    .map(line -> line.replaceAll(oldWord, newWord))
                    .collect(Collectors.toList());

            Files.write(pathOfFile, replaced);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
