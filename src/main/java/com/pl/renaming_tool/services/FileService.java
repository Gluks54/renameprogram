package com.pl.renaming_tool.services;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class FileService {
    public void copyFolder(File source, File dest) throws IOException {
        FileUtils.copyDirectory(source, dest);
    }
}
