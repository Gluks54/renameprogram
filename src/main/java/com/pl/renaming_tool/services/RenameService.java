package com.pl.renaming_tool.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class RenameService {
    public void rename(Path oldPath, String newName, String oldWord) throws IOException {
        String tempUri = oldPath
                .toString()
                .replaceAll(oldWord, newName);

        Path newPath = Paths.get(tempUri);

        Files.move(oldPath, newPath);
    }

    public String capitalize(String str)
    {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
