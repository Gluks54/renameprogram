package com.pl.renaming_tool.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XStreamAlias("rules")
public class Rules {
    @XStreamAlias("refactor-replace")
    RefactorReplace refactorReplace;
}
