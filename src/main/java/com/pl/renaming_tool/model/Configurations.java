package com.pl.renaming_tool.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.*;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@XStreamAlias("repository")
public class Configurations {

    @XStreamAlias("source-path")
    String sourcePath;

    @XStreamAlias("destination-path")
    String destinationPath;

    @XStreamImplicit
    private List<Rules> rulesList;
}
