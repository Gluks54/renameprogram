package com.pl.renaming_tool.configurations;

import com.pl.renaming_tool.model.Configurations;
import com.pl.renaming_tool.model.Rules;
import com.thoughtworks.xstream.XStream;

import java.io.File;

public class Reader {
    private final String confPath = "config.xml";

    public Configurations readConfig() {
        XStream xs = new XStream();
        xs.processAnnotations(Configurations.class);
        xs.processAnnotations(Rules.class);

        return (Configurations) xs.fromXML(new File(confPath));
    }
}
