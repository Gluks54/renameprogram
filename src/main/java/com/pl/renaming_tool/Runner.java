package com.pl.renaming_tool;

import com.pl.renaming_tool.configurations.Reader;
import com.pl.renaming_tool.model.RefactorReplace;
import com.pl.renaming_tool.processors.ClassAndContentProcessor;
import com.pl.renaming_tool.processors.PackageNameProcessor;
import com.pl.renaming_tool.services.FileService;
import com.pl.renaming_tool.services.RenameService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Runner {
    public static void main(String[] args) throws IOException {
        int replaceRuleOfList = 0;

        Reader reader = new Reader();
        FileService fileService = new FileService();
        RenameService renameService = new RenameService();
        PackageNameProcessor packageNameProcessor = new PackageNameProcessor();
        ClassAndContentProcessor classAndContentProcessor = new ClassAndContentProcessor();

        String sourcePath = reader.readConfig().getSourcePath();
        String targetPath = reader.readConfig().getDestinationPath();

        File fileSource = new File(sourcePath);
        File fileTarget = new File(targetPath);

        fileService.copyFolder(fileSource, fileTarget);

        Path targetProjectPath = Paths.get(targetPath + "/src");

        RefactorReplace refactorReplace = reader
                .readConfig()
                .getRulesList()
                .get(replaceRuleOfList)
                .getRefactorReplace();

        String oldWord = refactorReplace.getSource();
        String newWord = refactorReplace.getTarget();

        List<Path> pathListOfPackagesSmallFirstLetter = packageNameProcessor
                .findPackages(targetProjectPath, oldWord);

        List<Path> pathListOfPackagesBigFirstLetter = packageNameProcessor
                .findPackages(targetProjectPath, renameService.capitalize(oldWord));

        pathListOfPackagesSmallFirstLetter.forEach(x -> {
            try {
                renameService.rename(x, newWord, oldWord);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        pathListOfPackagesBigFirstLetter.forEach(x -> {
            try {
                renameService.rename(x, renameService.capitalize(newWord),
                        renameService.capitalize(oldWord));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        List<Path> listOfFiles = classAndContentProcessor
                .findFiles(targetProjectPath);

        listOfFiles.forEach(x ->
                classAndContentProcessor.renameContent(x, oldWord, newWord));

        listOfFiles.forEach(x ->
                classAndContentProcessor.renameContent(x, renameService.capitalize(oldWord),
                        renameService.capitalize(newWord)));

        List<Path> listOfClassesWithSmallFirstLetter = classAndContentProcessor
                .findClasses(targetProjectPath, oldWord);

        List<Path> listOfClassesWithBigFirstLetter = classAndContentProcessor
                .findClasses(targetProjectPath, renameService.capitalize(oldWord));

        listOfClassesWithSmallFirstLetter.forEach(x -> {
            try {
                renameService.rename(x, newWord, oldWord);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        listOfClassesWithBigFirstLetter.forEach(x -> {
            try {
                renameService.rename(x, renameService.capitalize(newWord),
                        renameService.capitalize(oldWord));

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
